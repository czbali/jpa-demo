package hu.braininghub.bh06.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.model.Alien;
import hu.braininghub.bh06.service.AlienService;

@WebServlet(urlPatterns = "/alienServlet")
public class AlienServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	AlienService as;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Alien a = as.getAlienbyId(Integer.parseInt(req.getParameter("aid")));

		System.out.println(a);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		/*
		 * Integer aid = Integer.parseInt((String) req.getParameter("aid")); String
		 * aname = req.getParameter("aname"); String tech = req.getParameter("tech");
		 * 
		 * 
		 * 
		 * Alien a = new Alien(); a.setAid(aid); a.setAname(aname); a.setTech(tech);
		 * 
		 * as.updateAlien(a, a.getAid());
		 * 
		 * System.out.println("alien has been updated");
		 */

		/*
		 * as.deleteAlienById(Integer.parseInt(req.getParameter("removeId")));
		 * 
		 * System.out.println("Alien with the " + req.getParameter("removeId") +
		 * " id has been removed");
		 */

		Integer aid = Integer.parseInt((String) req.getParameter("aid"));
		String aname = req.getParameter("aname");
		String tech = req.getParameter("tech");

		Alien a = new Alien();
		a.setAid(aid);
		a.setAname(aname);
		a.setTech(tech);

		as.createNewAlien(a);

		System.out.println("new alien has been created");

	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	}

}
