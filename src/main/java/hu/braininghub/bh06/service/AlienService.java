package hu.braininghub.bh06.service;

import hu.braininghub.bh06.model.Alien;

public interface AlienService {
	
	void createNewAlien(Alien alien);
	
	Alien getAlienbyId(Integer id);
	
	void deleteAlienById (Integer id);
	
	void updateAlien (Alien alien, Integer id);

}
