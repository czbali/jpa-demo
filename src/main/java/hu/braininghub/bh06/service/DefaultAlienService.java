package hu.braininghub.bh06.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import hu.braininghub.bh06.model.Alien;

@Stateless
public class DefaultAlienService implements AlienService {

	@PersistenceContext(unitName = "pu")
	private EntityManager em;

	public Alien getAlienbyId(Integer id) {
		return em.find(Alien.class, id);
	}

	public void createNewAlien(Alien alien) {
		em.persist(alien);
	}

	public void deleteAlienById(Integer id) {
		Alien a = em.find(Alien.class, id);
		em.remove(a);

	}

	public void updateAlien(Alien alien, Integer id) {
		
		
		Alien a = em.find(Alien.class, id);
		em.remove(a);
		em.persist(alien);
	
	}

}
